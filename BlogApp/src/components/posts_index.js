// This is a container but as this app is having more of containers no seperate folder is assigned
import React, {Component} from 'react';
import {connect} from 'react-redux';
// import {bindActionCreators} from 'redux';
// not needed
import {fetchPosts} from '../actions/index';
import {Link} from 'react-router';

class PostsIndex extends Component {
    // this method is called when the component is just about to be rendered to the dom for the FIRST time only
    componentWillMount() {
        //calling action creator
        this.props.fetchPosts();
        console.log(this.props.fetchPosts());
    }

    renderPosts(){
    //    our list of posts is available as an array this.props.posts
        return this.props.posts.map((post)=>{
            return <li className="list-group-item" key={post.id}>
                <Link to={`posts/${post.id}`}>
                    <span className="pull-xs-right">{post.categories}</span>
                    <strong>{post.title}</strong>
                </Link>
            </li>
        });
    }

    render() {
        return (
            <div>
                <div className="text-xs-right">
                    <Link to="/posts/new" className="btn btn-primary">
                        Add a Post
                    </Link>
                </div>
                <h3>Posts</h3>
                <ul className="list-group">
                    {this.renderPosts()}
                </ul>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {posts: state.posts.all};
}

// function mapDispatchToProps(dispatch){
//     return bindActionCreators({fetchPosts}, dispatch);
// }
// This function can be avoided by: { fetchPosts: fetchPosts } or just { fetchPosts } in connect as second arguement
export default connect(mapStateToProps, {fetchPosts})(PostsIndex);
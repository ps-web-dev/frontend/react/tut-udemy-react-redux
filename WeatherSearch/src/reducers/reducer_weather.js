import {FETCH_WEATHER} from '../actions/index';

export default function(state = [], action){
    /*  redux promise sees the incoming action and looks specifically the payload property.
        if payload is a promise then the redux promise which is a middleware will stop the action entirely.
        once the request finishes it will diapatch a new action of the same type but with a payload of the resolved request.
    */
   switch(action.type){
       case FETCH_WEATHER:
        // we are using array as we will be working with multiple cities.
        // as we need to add cities searched by user we need to push on to existing state.
        // concat doesn't mutate but returns a new instance of it (state)
        // return state.concat([action.payload.data]);
        // in ES6 we can write
        return [ action.payload.data, ...state ];
        // here we will be inserting to the top of the new array.
   }
    return state;
}
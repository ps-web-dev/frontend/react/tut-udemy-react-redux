import React from 'react';
import App from './components/app';
import {Route, IndexRoute} from 'react-router';
import PostsIndex from './components/posts_index';
import PostsNew from './components/posts_new';
import PostsShow from './components/posts_show';

export default(
    // react router just swaps the content in the page giving the user the impression that he is changing pages

    <Route path="/" component={App}>
        {/* IndexRoute is a helper like a Route and it will show when the url matches up with the path defined by the parent but not by one of the children */}
        {/* If the route matches the parent then show App and PostsIndex and if the route matches any childern routes show App and child route*/}
        <IndexRoute component={PostsIndex}/>
        {/** Greeting is nested inside App so we need to add code to render Greeting in the App*/}
        {/* Whenever we define a nested route, if we visit the nested route, the child component is passed to the parent as this.props.children */}
        <Route path="posts/new" component={PostsNew}/>
        {/*:id is a param. React router will identify that param and pass it to our component as this.props.params.id*/}
        <Route path="/posts/:id" component={PostsShow}/>
    </Route>
);